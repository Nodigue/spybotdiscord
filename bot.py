import random
import discord
from discord.ext import commands

# ----------------------------------------------------------------------------------------------------------------------- #
#                                                    STRUCTUTRE DU JEU                                                    #
# ----------------------------------------------------------------------------------------------------------------------- #

# ----------------------------------------------------------------------------------------------------- #
#                                           Fonctions Globales                                          #
# ------------------------------------------------------------------------------------------------------#

#-----------------------------------------------------------------------------------#
# Retourne le message correspondant au code dans la langue choisie                  #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (int) code : Code correspondant au message                                      #
# - (Game) game : Partie                                                            #
# - (string) [optional] m1 : Nom du premier utilisateur                             #
# - (string) [optional] m2 : Nom du deuxième utilisateur                            #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - (string) message / "..." : Message correspondant au code dans la langue choisie #
#-----------------------------------------------------------------------------------#
def getMessage(code, game, m1 = "", m2 = ""):

	try:

		# Code des messages personnells
		if (code == 41):
			m1_InGame, index = game.memberInGame(m1)

			# Récupération du joueur associé à l'utilisateur
			p1 = game.player_list[index]
			m1 = m1.display_name
			
			# Si le joueur est l'espion, on change le message personnel
			if (p1.isSpy):
				code += 1

		# Ouverture du fichier contenant les messages en fonction de la langue
		messages_file = open("messages_" + game.language + ".txt", "r")

		# Recherche du message correspondant au code
		message = messages_file.readline()

		while (not message.startswith(str(code))):
			message = messages_file.readline()

		# Fermeture du fichier
		messages_file.close()

		# Formatage du message
		message = message[6:len(message) - 1].replace("$1", m1).replace("$2", m2).replace("$3", game.location)

		return message
	
	except Exception as e:
		print(e)
		return "..."

# ----------------------------------------------------------------------------------------------------- #
#                                                 Classes                                               #
# ------------------------------------------------------------------------------------------------------#

# ----------------------------------------------------------------------------------------------------- #
#                                                 Player                                                #
# ----------------------------------------------------------------------------------------------------- #

class Player():

	#-----------------------------------------------------------------------------------#
	# Constructeur de la classe 'Player'                                                #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (discord.member) member : Utilisateur discord lié au joueur (player)            #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	def __init__(self, member):
		# Utilisateur Discord lié au joueur
		self.member = member

		# Si le joueur est l'espion ou non
		self.isSpy = False

		# Si le joueur a voté ou non
		self.hasVoted = False
		# Nombre de vote contre le joueur
		self.nbVoteAgainst = 0

		return

	#-----------------------------------------------------------------------------------#
	# Redéfinition de l'opéŕation '==' pour deux player                                 #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (player) player : Autre joueur à comparer                                       #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (boolean) Vrai : Si les deux joueurs sont les mêmes                             #
	#             Faux : Si les deux joueurs ne sont pas les mêmems                     #
	#-----------------------------------------------------------------------------------#
	def __eq__(self, player):
		# Comparaison des deux utilisateurs discord
		if (self.member == player):
			return True
		else:
			return False

	#-----------------------------------------------------------------------------------#
	# Retourne le nom du joueur (de l'utilisateur discord)                              #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (string) self.member.display_name : Nom du joueur (de l'utilisateur discord)    #
	#-----------------------------------------------------------------------------------#
	def getName(self):
		return self.member.display_name

# ----------------------------------------------------------------------------------------------------- #
#                                                  Game                                                 #
# ----------------------------------------------------------------------------------------------------- #

class Game():

	#-----------------------------------------------------------------------------------#
	# Constructeur de la classe 'Game'                                                  #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	def __init__(self):
		# Réinitialisation de la partie
		self.reset()
		
		return

	#-----------------------------------------------------------------------------------#
	# Réinitialise la partie avec des paramètres par défaut                             #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	def reset(self):
		# Langue des messages
		self.language = "fr"
		# État de la partie (0 : Non créé /  1 : En création / 2 : Lancée)
		self.state = 0                                                        
		
		# Liste des joueurs
		self.player_list = []
		# Lieu de la partie (celui que doit trouver l'espion)
		self.location = ""

		return

	#-----------------------------------------------------------------------------------#
	# Crée une nouvelle partie                                                          #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) 10 / -10 / -12 : Code du message à afficher                               #
	#-----------------------------------------------------------------------------------#
	def createNewGame(self):

		if (self.state == 0):
			self.state = 1
			return 10

		elif (self.state == 1):
			return -10

		elif (self.state == 2):
			return -12

	#-----------------------------------------------------------------------------------#
	# Vérifie si un utilisateur discord est dans la partie                              #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (discord.member) member : Utilisateur discord à vérifier                        #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (boolean) inGame : Vrai si l'utilisateur est dans la partie                     #
	#                      Faux sinon                                                   #
	# - (int) index : Position de l'utilisateur dans la liste des joueurs               #
	#-----------------------------------------------------------------------------------#
	def memberInGame(self, member):
	
		inGame = False
		index = -1

		for i in range (0, len(self.player_list)):
			if (self.player_list[i] == member):
				
				inGame = True
				index = i

		return inGame, index

	#-----------------------------------------------------------------------------------#
	# Ajoute un utilisateur discord à la liste des joueurs                              #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (discord.member) member : Utilisateur discord à ajouter                         #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) -11 / -12 / -20 / 20 : Code du message à afficher                         #
	#-----------------------------------------------------------------------------------#
	def addPlayer(self, member):
		
		if (self.state == 0):
			return -11

		elif (self.state == 1):
			inGame, index = self.memberInGame(member)

			if (not inGame):
				self.player_list.append(Player(member))
				return 20

			else:
				return -20

		elif (self.state == 2):
			return -12

	#-----------------------------------------------------------------------------------#
	# Retire un utilisateur discord de la liste des joueurs                             #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (discord.member) member : Utilisateur discord à retirer                         #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) -11 / -12 / -21 / 21 : Code du message à afficher                         #
	#-----------------------------------------------------------------------------------#
	def removePlayer(self, member):

		if (self.state == 0):
			return -11

		elif (self.state == 1):
			inGame, index = self.memberInGame(member);

			if (inGame):
				del self.player_list[index]
				return 21

			else:
				return -21

		elif (self.state == 2):
			return -12

	#-----------------------------------------------------------------------------------#
	# Retourne le nombre de joueur dans la partie                                       #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) len(self.player_list) : Nombre de joueur dans la partie                   #
	#-----------------------------------------------------------------------------------#
	def getNbPlayer(self):
		return len(self.player_list)

	#-----------------------------------------------------------------------------------#
	# Lance la partie                                                                   #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) -11 / -12 / -14 / 11 : Code du message à afficher                         #
	#-----------------------------------------------------------------------------------#
	def startGame(self):
		if (self.state == 0):
			return -11

		elif (self.state == 1):

			if (len(self.player_list) < 2):
				return -14
			
			else:
				self.state = 2
				return 11

		elif (self.state == 2):
			return -12

	#-----------------------------------------------------------------------------------#
	# Choisi un lieu au hasard parmi les lieux présent dans le fichier 'location.txt'   #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	def pick_location(self):
		locations_file = open("locations.txt", "r")
		locations = locations_file.readlines()
		locations_file.close()

		self.location = locations[random.randint(0, len(locations) - 1)]
		self.location = self.location[:len(self.location) - 1]

		return

	#-----------------------------------------------------------------------------------#
	# Choisi l'espion au hasard parmi la liste des joueurs                              #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	def pick_spy(self):
		self.player_list[random.randint(0, len(self.player_list) - 1)].isSpy = True
		return

	#-----------------------------------------------------------------------------------#
	# Prend en compte le vote de l'utilisateur m1 contre l'utilisateur m2               #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - (discord.member) m1 : Utilisateur discord votant                                #
	# - (discord.member) m2 : Utilisateur discord cible du vote                         #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) -11 / -13 / -30 / -31 / -32 / -33 / -34 / 30 : Code du message à afficher #
	#-----------------------------------------------------------------------------------#
	def vote(self, m1, m2):

		# Vérification de l'état du jeu
		if (self.state == 0):
			return -11

		elif (self.state == 1):
			return -13

		else:

			# Vérification que l'utilisateur m1 est bien dans la partie
			m1_InGame, index_1 = self.memberInGame(m1)
			
			if (not m1_InGame):
				return -32

			else:

				# Vérification que l'utilisateur ne vote pas contre lui même ou contre le maitre du jeu (le bot)
				if (m1 == m2):
					return -30

				elif (m2 == bot.user):
					return -31

				else:

					#Vérification que l'utilisateur m2 est bien dans la partie
					m2_InGame, index_2 = self.memberInGame(m2)

					if (not m2_InGame):
						return -33
					
					else:

						# Recherche des joueurs associés aux utilisateurs
						p1 = self.player_list[index_1]
						p2 = self.player_list[index_2]

						# Vérification que le joueur p1 (utilisateur m1) n'a pas déjà voté
						if (not p1.hasVoted):

							#Considération du vote
							p1.hasVoted = True
							p2.nbVoteAgainst += 1

						else:
							return -34

		return 30

	#-----------------------------------------------------------------------------------#
	# Retourne le nombre de personne ayant déjà voté                                    #
	#-----------------------------------------------------------------------------------#
	# Paramètre(s):                                                                     #
	# - X                                                                               #
	#-----------------------------------------------------------------------------------#
	# Retourne:                                                                         #
	# - (int) nbVote : Nombre de personne ayant déjà voté                               #
	#-----------------------------------------------------------------------------------#
	def getNbVote(self):
		
		nbVote = 0

		for i in range (0, len(self.player_list)):
			if (self.player_list[i].hasVoted):
				nbVote += 1

		return nbVote

# ----------------------------------------------------------------------------------------------------------------------- #
#                                                           BOT                                                           #
# ----------------------------------------------------------------------------------------------------------------------- #

bot = commands.Bot(command_prefix = '&')

# ----------------------------------------------------------------------------------------------------- #
#                                               Evénements                                              #
# ------------------------------------------------------------------------------------------------------#

#-----------------------------------------------------------------------------------#
# S'exécute au lancement du bot                                                     #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.event
async def on_ready():
	
	bot.game = Game()
	return

# ----------------------------------------------------------------------------------------------------- #
#                                               Commandes                                               #
# ------------------------------------------------------------------------------------------------------#

#-----------------------------------------------------------------------------------#
# Commande : &new -> Créer une partie                                               #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (discord.ctx) ctx : Contexte de la commande (Voir Discord API)                  #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.command(name = 'new')
async def new_game(ctx):

	e = bot.game.createNewGame()
	await ctx.send(getMessage(e, bot.game))

	return

#-----------------------------------------------------------------------------------#
# Commande : &join -> Rejoindre la partie                                           #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (discord.ctx) ctx : Contexte de la commande (Voir Discord API)                  #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.command(name = 'join')
async def join_game(ctx):

	e = bot.game.addPlayer(ctx.author)
	await ctx.send(getMessage(e, bot.game, ctx.author.display_name))

	# Si le joueur a bien rejoint la partie 
	if (e == 20):
		
		# Récapitulatif des joueurs dans la partie
		if (len(bot.game.player_list) > 0):

			await ctx.send(getMessage(40, bot.game))

			for i in range(0, len(bot.game.player_list)):
				await ctx.send("- " + bot.game.player_list[i].getName())

	return

#-----------------------------------------------------------------------------------#
# Commande : &quit -> Quitter la partie                                             #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (discord.ctx) ctx : Contexte de la commande (Voir Discord API)                  #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.command(name = 'quit')
async def quit_game(ctx):

	e = bot.game.removePlayer(ctx.author)
	await ctx.send(getMessage(e, bot.game, ctx.author.display_name))

	# Si le joueur a bien quitté la partie
	if (e == 21):

		# Récapitulatif des joueurs dans la partie
		if (bot.game.getNbPlayer() > 0):

			await ctx.send(getMessage(40, bot.game))

			for i in range(0, bot.game.getNbPlayer()):
				await ctx.send("- " + bot.game.player_list[i].getName())

	return

#-----------------------------------------------------------------------------------#
# Commande : &start -> Lance la partie                                              #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (discord.ctx) ctx : Contexte de la commande (Voir Discord API)                  #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.command(name = 'start')
async def start_game(ctx):
	
	e = bot.game.startGame()
	await ctx.send(getMessage(e, bot.game))

	# Vérification que la partie a bien été créée
	if (e == 11):

		# Choix du lieu
		bot.game.pick_location()
		# Choix de l'espion
		bot.game.pick_spy()

		# Envoi de messages personnelles à chaque joueur
		for i in range (0, bot.game.getNbPlayer()):
			member = bot.game.player_list[i].member
			await member.send(getMessage(41, bot.game, member))

	return

#-----------------------------------------------------------------------------------#
# Commande : &vote -> Vote contre un joueur                                         #
#-----------------------------------------------------------------------------------#
# Paramètre(s):                                                                     #
# - (discord.ctx) ctx : Contexte de la commande (Voir Discord API)                  #
# - (discord.Member) member : Membre contre qui l'on souhaite voter                 #
#-----------------------------------------------------------------------------------#
# Retourne:                                                                         #
# - X                                                                               #
#-----------------------------------------------------------------------------------#
@bot.command(name = 'vote')
async def vote(ctx, *, member: discord.Member):
	
	try:
		e = bot.game.vote(ctx.author, member)
		await ctx.send(getMessage(e, bot.game, ctx.author.display_name, member.display_name))

		# Récapitulatif des votes
		if (bot.game.getNbVote() > 0):
			await ctx.send(getMessage(43, bot.game))

			for i in range (0, bot.game.getNbPlayer()):
				await ctx.send(bot.game.player_list[i].getName() + " : " + str(bot.game.player_list[i].nbVoteAgainst))

			# Si tout le monde a voté
			if (bot.game.getNbVote() == bot.game.getNbPlayer()):
				await ctx.send(getMessage(44, bot.game))

	except Exception as e:
		print(e)

	return

# ----------------------------------------------------------------------------------------------------------------------- #

bot.run('MjI1MzkzMjIxNjk2NDg3NDI0.Xm5dBg.1q48_4OiguBOg_8hKynzjfIdN_0')